#!/usr/bin/env python3

"""
Protein allignment
"""


__author__ = "K.A. Notebomer"
__status__ = "Test Module"
__version__ = "0.1"

import copy
import sys
import os
import traceback
import logging
import math
import argparse
from Bio.Align.Applications import ClustalOmegaCommandline
from Bio import AlignIO
from Bio import SeqIO
import warnings
from Bio import BiopythonWarning


class Position:
    """
    Data class to store the data for a single point in the sequence
    """
    def __init__(self, found_aa: list, new_aa: str):
        self.all: list = found_aa
        self.new_aa = new_aa
        self.unique_possible: set = set(self.all)
        self.most_common: str = self.get_most_common()
        self.log_score = self.get_log_score()
        self.final_score = self.get_final_score()

    def get_log_score(self) -> float:
        log_score = math.log(len(self.unique_possible), 10)
        return log_score

    def get_most_common(self) -> str:
        most_common_aa = max(set(self.all), key=self.all.count)
        return most_common_aa

    def get_final_score(self) -> int:
        if self.new_aa in self.all:
            final_score = 1
        else:
            final_score = (self.log_score - 0) / (math.log(len(self.all)) - 0) * 10
            final_score = 10 - final_score
        return math.floor(final_score)


def make_msa(filename: str, clustal0_location: str):
    """
    Makes an MSA of the sequences in the given file
    :param filename: name of file
    :param clustal0_location: location of clustalO
    :return: 0
    """

    clustalo_exe = rf"{clustal0_location}"
    clustalo_cline = ClustalOmegaCommandline(clustalo_exe, infile=filename, outfile="out.aln", verbose=True, auto=False,
                                             force=True, outfmt="clu")

    assert os.path.isfile(clustalo_exe), "Clustal Omega executable missing"
    stdout, stderr = clustalo_cline()

    return 0


def read_gene_seqeunce(file, filetype):
    """
    Read the gene sequence
    :param file: file with gene
    :param filetype: type of file
    :return: object
    """
    gene = None
    for seq_record in SeqIO.parse(file, filetype):
        gene = seq_record

    return gene


def align(snp_induced_gene, original_gene, aa_position: int, clustalO: str):
    """
    Align translated gene with SNP to MSA
    :param snp_induced_gene: object
    :param original_gene: object
    :param aa_position: int
    :param clustalO: ClustalO location
    :return: 0
    """
    original_translated_sequence = original_gene.seq.transcribe().translate(to_stop=True)
    snp_induced_gene.seq = snp_induced_gene.seq.transcribe().translate(to_stop=True)

    if original_translated_sequence[aa_position] == snp_induced_gene.seq[aa_position]:
        print("Snp is synonymous. \n"
              "Score: 0\n"
              "Exiting program")
        sys.exit(0)

    if snp_induced_gene.seq == "":
        print("The snp that was introduced, induces an early termination in the start of the protein. \n"
              "Score: 10 \n"
              "Stopping program")
        sys.exit(0)

    print(f"The new amino acid on position {aa_position}, "
          f"changed from {original_translated_sequence[aa_position]} to {snp_induced_gene.seq[aa_position]}")

    SeqIO.write(snp_induced_gene, "SNPGene.fasta", "fasta")

    clustalo_exe = rf"{clustalO}"
    clustalo_cline = ClustalOmegaCommandline(clustalo_exe, profile1="out.aln", profile2="SNPGene.fasta",
                                             outfile="out2.aln", verbose=True, auto=False,
                                             force=True, outfmt="clu")

    assert os.path.isfile(clustalo_exe), "Clustal Omega executable missing"
    stdout, stderr = clustalo_cline()

    return 0


def introduce_snp(gene: 'Bio.SeqRecord.SeqRecord', place: int, new_nuc: str) -> 'Bio.SeqRecord.SeqRecord':
    """
    Introduce an SNP in the givne DNA sequence
    :param gene: Object
    :param place: int
    :param new_nuc: String
    :return: Object
    """

    if place > len(gene.seq):
        print("Position outside of range of gene.")
        print("Stopping program")
        sys.exit(1)
    new_nuc = new_nuc.upper()
    if gene.seq[place] == new_nuc:
        print("This is the same nucleotide that is already present in the sequence,"
              " please enter a nucleotide that is an actual snp.")
        print("Stopping program")
        sys.exit(0)

    snp_gene = copy.deepcopy(gene)
    snp_gene.seq = gene.seq[0: place] + new_nuc + gene.seq[place + 1:]

    return snp_gene


def score_snp(position_aa: int) -> int:
    """
    Score the snp and print to the screen
    :param position_aa: int
    :return: o
    """

    align = AlignIO.read("out2.fasta", "clustal")
    relative_position = get_relative_position(position=position_aa, alignment=align)
    position_data = Position([aa for aa in align[:-1, relative_position]], align[-1:, relative_position])
    print(f"The score of the snp: {position_data.final_score}")

    return 0


def get_relative_position(position: int, alignment: object) -> int:
    """
    Get the relative position of the selected protein in the aligned sequence
    :param position: int
    :param alignment: Object
    :return: int
    """
    # The new index in the alignment
    new_index = 0

    # The index, ignoring gaps
    amino_acid_index = 0

    for place in alignment[-1].seq:
        if place != "-":
            if amino_acid_index == position:
                return new_index
            amino_acid_index += 1
        new_index += 1


def argparser():
    """
    Parse all command line arguments
    :return: args
    """
    parser = argparse.ArgumentParser(description='Process the given arguments from the command line')
    parser.add_argument('-c', '--clustalo', type=str,
                        help='Location of clustalO', required=True)
    parser.add_argument('-m', '--msa', type=str,
                        help='The multi fasta file to use to make the MSA', required=True)
    parser.add_argument('-g', '--gene', type=str,
                        help='File containing the gene sequence', required=True)
    parser.add_argument('-f', '--filetype', choices=['genbank', 'fasta'],
                        help='Filetype of gene', required=True)
    parser.add_argument('-p', '--position', type=int,
                        help='Position to introduce the SNP', required=True)
    parser.add_argument('-n', '--nucleotide', choices=['A', 'C', 'T', 'G',
                                                       'a', 'c', 't', 'g'],
                        help='New nucleotide to introduce', required=True)

    args = parser.parse_args()
    return args


def main():
    """
    Main function that runs everything
    :return: 0
    """
    try:
        warnings.simplefilter('ignore', BiopythonWarning)
        args = argparser()
        position = args.position
        position -= 1
        make_msa(args.msa, args.clustalo)
        gene = read_gene_seqeunce(args.gene, args.filetype)
        snp_gene = introduce_snp(gene, position, "g")

        position_aa = position // 3

        align(snp_gene, gene, position_aa, args.clustalo)
        score_snp(position_aa)
    except FileNotFoundError as e:
        print(f"Error {e}, {type(e)}")
    except Exception as e:
        logging.error(traceback.format_exc())

    return 0


if __name__ == "__main__":
    sys.exit(main())
