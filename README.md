# README #

This project is a python script that can make an msa and can score and snp in a related sequence.


### Steps undertaken by the program ###

1. Take a bunch of closely related protein sequences and make an msa.
2. Introduce an SNP in the given gene.
3. Translate the sequence to it's corresponding protein.
4. Align this new sequence to the msa.
5. Score the SNP, based on conservation.

### Non standard libraries ###

* argparse
* Bio (Biopython)

### Arguments ###

| Parameter         | Description                                                           |
| ---               | ---                                                                   |
| -c --clustalo     | clustalO location                                                     |
| -m --msa          | the multi fasta file to use to make the MSA                           |
| -g --gene         | file containing the gene sequence                                     |
| -f --filetype     | filetype of gene, must be 'genbank' or  'fasta'                       |
| -p --position     | position to introduce the SNP, normal counting (not pythonic)         |
| -n --nucleotide   | new nucleotide to introduce; must be A, C, T or G; case insensitive   |                     |

### Example command ###

This is an example of the command you could run.
```
python snp_opdracht_bin3.py 
-c C:Users\kaspe\Desktop\clustal-omega-1.2.2-win64\clustal-omega-1.2.2-win64\clustalo.exe 
-m homologene_result2.txt 
-g sequence_human.gb 
-f genbank 
-p 106 
-n g
```

On the bin linux systems you would need to change the location of clustalO.
I am not certain whether this command works, I haven't tested it.
```
python3 snp_opdracht_bin3.py
-c /usr/bin/clustalo
-m homologene_result2.txt
-g sequence_human.gb
-f genbank
-p 106
-n g
```

The data that is used in this example command is of the ITIH6 gene. 
More info can be found at [https://www.ncbi.nlm.nih.gov/homologene/89309](https://www.ncbi.nlm.nih.gov/homologene/89309).

The gene that the SNP is introduced into in this example is the ITIH6 gene
 found in Homo sapiens and can be found at [https://www.ncbi.nlm.nih.gov/nuccore/NM_198510.3](https://www.ncbi.nlm.nih.gov/nuccore/NM_198510.3).

### Result description ###

The program outputs a score of 1 to 10. 10 means that the introduced snp is probably bad, 
while a 1 means that the mutation is probably neutural.

The results are calculated by log10 transforming the amount of unique amino acids a specific place. 
This score is then converted using the same function as min max scaling and multiplied by 10, 
the result is then subtracted from 10 to get the final score. If the amino acid is already present
in the MSA then the score will automatically be set to 1.

The reason we log10 transform the amount of unique amino acids in a specific position is because this gives a 
good scale on how bad an SNP would be. The more unique amino acids in a location the more likely it is that 
the SNP isn't deleterious. If the SNP gives an amino acid that is already present in the the location the score
is automatically set to zero because there is already a functioning protein with the same amino acid in that 
position, which means that the mutation is probably neutral.
It is also important to mention that the score is kind of min max scaled (uses the same type of calculation,
max being the amount of sequences in the given MSA) and them multiplied by 10.
This is to give a relative score based on the amount of sequences that are actually present in the MSA, and so the score can be between 1 and 10.

### Contact ###

* K. Notebomer
* K.a.notebomer@st.hanze.nl